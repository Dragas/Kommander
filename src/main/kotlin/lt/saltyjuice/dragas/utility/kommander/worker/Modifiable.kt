package lt.saltyjuice.dragas.utility.kommander.worker

import lt.saltyjuice.dragas.utility.kommander.annotations.Description
import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier
import lt.saltyjuice.dragas.utility.kommander.exception.KommanderException
import lt.saltyjuice.dragas.utility.kommander.main.Describable
import java.lang.reflect.AccessibleObject
import java.lang.reflect.Member

/**
 * Basic wrapper for any fields and methods used by Workers to build themselves each time they're invoked.
 */
abstract class Modifiable(protected val accessible: AccessibleObject) : Describable
{
    val modifiers: List<String> = mutableListOf()
    override val description: String by lazy()
    {
        val annotation = accessible.getAnnotation(Description::class.java)
        val descriptionBuilder = StringBuilder()
        var modifiersString = modifiers.filter(String::isNotBlank).joinToString("|-")
        if (modifiersString.isNotBlank())
            modifiersString = "\t-$modifiersString"
        descriptionBuilder.append(modifiersString)
        val parameter = parameterType()
        if (parameter.isNotBlank())
            descriptionBuilder.append(" [$parameter]")
        descriptionBuilder
                .append(" ")
                .append(annotation?.value ?: "")
        descriptionBuilder.toString()
    }
    init
    {
        modifiers as MutableList<String>
        modifiers.addAll(parseModifiers())
        accessible.isAccessible = true
    }

    protected fun parseModifiers(): List<String>
    {
        val annotation = accessible.getAnnotation(Modifier::class.java)
        val merged = annotation.alternatives.toMutableList()
        merged.add(0, annotation.name)
        return merged
    }

    /**
     * Casts given value to provided clazz. If clazz isn't primitive, a KommanderException is thrown.
     *
     * @param clazz class to cast the given value to
     * @param value something to cast
     * @throws KommanderException when [clazz] isn't that of primitive.
     */
    @Throws(KommanderException::class)
    protected open fun cast(clazz: Class<*>, value: Any?): Any?
    {
        if (clazz == Boolean::class.java && value == null)
            return true
        return when (clazz)
        {
            String::class.java -> value.toString()
            Char::class.java -> value.toString()[0]
            Byte::class.java -> value.toString().toByte()
            Short::class.java -> value.toString().toShort()
            Int::class.java -> value.toString().toInt()
            Long::class.java -> value.toString().toLong()
            Double::class.java -> value.toString().toDouble()
            Float::class.java -> value.toString().toFloat()
            Boolean::class.java -> value.toString().toBoolean()
            else -> throw KommanderException("${clazz.canonicalName} is not primitive")
        }
    }

    abstract fun parameterType(): String

    /**
     * Handles execution of particular [Modifiable] object.
     *
     * @param obj an object on which this modifiable is supposed to be invoked on.
     * @param value values with which this modifiable is executed
     * @throws KommanderException when any of the values fails to get cast
     */
    @Throws(KommanderException::class)
    abstract fun execute(obj: Any, vararg value: Any)

    open fun type(): String
    {
        return accessible.javaClass.name
    }

    override fun toString(): String
    {
        if (accessible is Member)
            return accessible.toString()
        return super.toString()
    }
}