package lt.saltyjuice.dragas.utility.kommander.exception

class WorkerBuilderException(message : String) : Exception(message)
{
}