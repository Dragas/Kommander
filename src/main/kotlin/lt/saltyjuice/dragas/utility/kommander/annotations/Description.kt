package lt.saltyjuice.dragas.utility.kommander.annotations

/**
 * Annotates particular Modifier or Command with description
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.CLASS)
annotation class Description(val value: String)