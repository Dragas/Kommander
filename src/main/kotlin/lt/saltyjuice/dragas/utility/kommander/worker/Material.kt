package lt.saltyjuice.dragas.utility.kommander.worker

import java.lang.reflect.Field

/**
 * Wrapper object for annotated fields in Command. Workers use these to build themselves
 *
 */
open class Material(@get:Synchronized protected val field: Field) : Modifiable(field)
{
    protected val clazz = field.type

    override fun parameterType(): String
    {
        return if (isBoolean()) "" else field.type.simpleName
    }

    protected fun isBoolean(): Boolean
    {
        return clazz.isAssignableFrom(Boolean::class.java)
    }

    override fun execute(obj: Any, vararg value: Any)
    {
        val cast = if (isBoolean())
            true
        else
            cast(clazz, value[0])
        field.set(obj, cast)
    }
}