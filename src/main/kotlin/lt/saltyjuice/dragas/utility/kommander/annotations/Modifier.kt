package lt.saltyjuice.dragas.utility.kommander.annotations

/**
 * Notes that this function is used as a modifier. Should only be applied to boolean properties.
 * @param name modifier's name, for example "f" or "v"
 * @param alternatives alternative names for this modifier.
 * For example "f" verbose modifier would be "-force", when separator is " -"
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_SETTER)
annotation class Modifier(val name: String, vararg val alternatives: String)