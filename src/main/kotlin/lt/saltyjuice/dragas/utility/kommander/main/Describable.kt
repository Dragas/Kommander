package lt.saltyjuice.dragas.utility.kommander.main

/**
 * Permits describing a particular object.
 */
interface Describable
{
    val description: String
}