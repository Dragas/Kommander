package lt.saltyjuice.dragas.utility.kommander.main

import lt.saltyjuice.dragas.utility.kommander.exception.KommanderException
import lt.saltyjuice.dragas.utility.kommander.exception.WorkerBuilderException
import lt.saltyjuice.dragas.utility.kommander.worker.Worker

/**
 * Default Kommander implementation.
 *
 * The default constructor accepts any amount of "[Command]" that can be used by this Kommander instance.
 * Since Kommanders aren't singletons, you may have multiple Kommanders that contain different commands.
 *
 * Each time a corresponding [Command] is found, a new instance of it is created, meaning that they work as both
 * "Builders" and "Executors" for necessary arguments.
 */
open class Kommander constructor(private vararg val clazz: Class<out Command>) : Describable
{
    override val description: String by lazy()
    {
        commands.map { it.value.description }.joinToString(System.lineSeparator().repeat(2))
    }
    protected val commands: MutableMap<String, Worker> = HashMap()

    @Throws(WorkerBuilderException::class, KommanderException::class)
    open fun initialize(): Kommander
    {
        clazz
                .map(this::builder)
                .map(Worker.Builder::build)
                .forEach(this::consume)
        return this
    }

    protected open fun builder(clazz: Class<out Command>): Worker.Builder
    {
        return Worker.Builder(clazz)
    }

    @Throws(KommanderException::class)
    protected open fun consume(worker: Worker)
    {
        val name = worker.obtainName()
        if (commands.containsKey(name))
            throw KommanderException("Another command shadowed: $name")
        commands[name] = worker
    }

    @Throws(KommanderException::class)
    open fun execute(commandLine: String)
    {
        val commandLineSplit = commandLine.split(" ", limit = 2).toMutableList()
        var arguments = commandLineSplit.getOrElse(1, { _ -> "" })
        val firstArgument = commandLineSplit[0]
        var command: Worker? = commands[firstArgument]
        if (command == null)
        {
            command = commands[""]
            arguments = commandLine
        }
        command ?: throw KommanderException("No such command: '$firstArgument'")
        command.execute(arguments)
    }
}