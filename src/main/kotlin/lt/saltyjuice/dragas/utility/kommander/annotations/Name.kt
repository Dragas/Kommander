package lt.saltyjuice.dragas.utility.kommander.annotations

/**
 * Permits overriding default name for Command class
 */
@Target(AnnotationTarget.CLASS)
annotation class Name(val value: String)