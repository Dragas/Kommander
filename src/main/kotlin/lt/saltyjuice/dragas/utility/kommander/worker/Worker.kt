package lt.saltyjuice.dragas.utility.kommander.worker

import lt.saltyjuice.dragas.utility.kommander.annotations.Description
import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier
import lt.saltyjuice.dragas.utility.kommander.annotations.Name
import lt.saltyjuice.dragas.utility.kommander.exception.KommanderException
import lt.saltyjuice.dragas.utility.kommander.exception.WorkerBuilderException
import lt.saltyjuice.dragas.utility.kommander.main.Command
import lt.saltyjuice.dragas.utility.kommander.main.Describable
import java.lang.reflect.AnnotatedElement
import java.lang.reflect.Field
import java.lang.reflect.Method
import kotlin.streams.toList

/**
 * Works like a builder like instance that maps any provided modifiers to command's methods.
 *
 * Every time a worker is executed, it creates a new instance of provided [command].
 */
open class Worker(protected val command: Class<out Command>) : Describable
{
    protected open val defaultSeparator = Regex("( -)|(^-)")
    protected open var modifiers: Map<String, Modifiable> = mapOf()
    protected open var name: String = ""
    protected open var commandDescription: String = ""

    override val description: String by lazy()
    {
        val descriptionBuilder = StringBuilder()
        descriptionBuilder
                .append(name)
        if (commandDescription.isNotBlank())
            descriptionBuilder.append(" - ")
        descriptionBuilder
                .appendln(commandDescription)
                .append(modifiers.values.distinct().map { it.description }.joinToString("\n"))
        descriptionBuilder.toString()
    }

    open fun obtainName(): String
    {
        return name
    }

    @Throws(KommanderException::class)
    open fun execute(line: String)
    {
        val commandInstance = command.newInstance()
        val splitArguments = line.split(defaultSeparator)
        splitArguments
                .filter(String::isNotBlank)
                .forEach { parseModifier(commandInstance, it) }
        if (commandInstance.validate())
            commandInstance.execute()

    }

    open fun execute(args: Array<String>)
    {
        val merged = args.joinToString(" ")
        execute(merged)
    }

    @Throws(KommanderException::class)
    protected open fun parseModifier(command: Command, arg: String)
    {
        var arguments = arg.split(" ", limit = 2)
        var toDrop = 1
        val modifierName = arguments[0]
        var method = modifiers[modifierName]
        if (method == null)
        {
            method = modifiers[""]
            arguments = listOf("", arg)
        }
        arguments = arguments.drop(toDrop)
        try
        {
            method?.execute(command, *arguments.toTypedArray())
        }
        catch (err: Exception)
        {
            throw KommanderException("While parsing modifiers for $method", err)
        }
    }

    open class Builder(protected val clazz: Class<out Command>)
    {
        protected open var name: String = (clazz.getAnnotation(Name::class.java)?.value ?: clazz.simpleName).toLowerCase()

        protected open var commandDescription: String = clazz.getAnnotation(Description::class.java)?.value ?: ""

        /**
         * Allows overriding worker's default name
         */
        open fun workerName(name: String): Builder
        {
            this.name = name
            return this
        }

        open fun workerDescription(description: String): Builder
        {
            this.commandDescription = description
            return this
        }

        /**
         * Builds a worker from provided [clazz].
         *
         * @return [Worker] that can be later used by itself or as part of Kommander
         */
        @Throws(WorkerBuilderException::class)
        open fun build(): Worker
        {
            var clazz = this.clazz as? Class<out Any?>
            val declaredFields = mutableListOf<Field>()
            val declaredMethods = mutableListOf<Method>()
            while (clazz != null)
            {
                declaredFields.addAll(clazz.declaredFields)
                declaredMethods.addAll(clazz.declaredMethods)
                clazz = clazz.superclass
            }
            val tools = declaredMethods
                    .parallelStream()
                    .filter(this::filterForAnnotation)
                    .filter(this::filterForParameters)
                    .map(this::provideTool)
                    .toList()
            val materials = declaredFields
                    .parallelStream()
                    .filter(this::filterForAnnotation)
                    .map(this::provideMaterial)
                    .toList()
            val worker = provideWorker(this.clazz)
            worker.name = this.name
            worker.modifiers = merge(tools, materials)
            worker.commandDescription = this.commandDescription
            return worker
        }

        /**
         * Merges provided tools and materials by prioritising materials.
         */
        @Throws(WorkerBuilderException::class)
        private fun merge(tools: List<Tool>, materials: List<Material>): Map<String, Modifiable>
        {
            val hm = HashMap<String, Modifiable>()
            merge(hm, materials)
            merge(hm, tools)
            return hm
        }

        @Throws(WorkerBuilderException::class)
        private fun merge(map: MutableMap<String, Modifiable>, modifiable: List<Modifiable>)
        {
            val mapped = modifiable.flatMap() { modifier -> modifier.modifiers.map { Pair(it, modifier) } }
            mapped.forEach()
            { (key, value) ->
                val result = map.putIfAbsent(key, value)
                if (result != null)
                    throw WorkerBuilderException("Modifier '$key' with '$value' shadows another modifier with '$result'")
            }
        }

        protected open fun provideWorker(clazz: Class<out Command>): Worker
        {
            return Worker(clazz)
        }

        protected open fun provideTool(method: Method): Tool
        {
            return Tool(method)
        }

        protected open fun filterForAnnotation(method: AnnotatedElement): Boolean
        {
            return method.getAnnotation(Modifier::class.java) != null
        }

        protected open fun provideMaterial(field: Field): Material
        {
            return Material(field)
        }

        @Throws(WorkerBuilderException::class)
        protected open fun filterForParameters(method: Method): Boolean
        {
            val params = method.parameters
            if (params.isEmpty())
                return true
            val firstParameterIsPrimitive = params[0].type.isPrimitive || String::class.java.isAssignableFrom(params[0].type)
            if (!firstParameterIsPrimitive)
            {
                throw WorkerBuilderException("First parameter for ${method.toGenericString()} is not a primitive value or a string")
            }/*
        val subsequentParametersAreNullable = params.toList().drop(1).all { it.getAnnotation(Nullable::class.java) != null }
        if (!subsequentParametersAreNullable)
        {
            throw WorkerBuilderException("Each parameter beyond the first for method ${method.toGenericString()} should be nullable (Or have @org.jetbrains.annotations.Nullable annotation, if you're doing java)")
        }*/
            return firstParameterIsPrimitive /*&& subsequentParametersAreNullable*/
        }

        /*@Throws(WorkerBuilderException::class)
        protected open fun mapMethods(method: Method)
        {
            val annotation = method.getAnnotation(Modifier::class.java)
            val mergedList = annotation.alternatives.toMutableList()
            mergedList.add(annotation.name)
            if (!mergedList.any(modifiers::containsKey))
            {
                mergedList.forEach { modifiers[it] = method }
            }
            else
            {
                val modifierName = mergedList.first(modifiers::containsKey)
                val shadowedMethod = modifiers[modifierName]!!
                throw WorkerBuilderException("Modifier $modifierName for method ${method.toGenericString()} shadows another method ${shadowedMethod.toGenericString()}")
            }
        }*/
    }
}