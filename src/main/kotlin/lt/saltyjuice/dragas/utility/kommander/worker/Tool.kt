package lt.saltyjuice.dragas.utility.kommander.worker

import java.lang.reflect.Method
import java.lang.reflect.Parameter

/**
 * Wrapper object for methods that are used by Workers to build their commands during each execution.
 */
open class Tool(protected val method: Method) : Modifiable(method)
{
    override fun parameterType(): String
    {
        return method.parameterTypes.getOrNull(0)?.simpleName ?: ""
    }

    override fun execute(obj: Any, vararg value: Any)
    {
        val parameters = method.parameters
        val casted = parameters
                .mapIndexed { index: Int, any: Parameter ->
                    cast(any.type, value.getOrNull(index))
                }
                .toMutableList()
        method.invoke(obj, *casted.toTypedArray())
    }
}