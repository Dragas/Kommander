package lt.saltyjuice.dragas.utility.kommander.main

import lt.saltyjuice.dragas.utility.kommander.annotations.Name

/**
 * Basic abstraction for any commands use by [Kommander]. By default, Commands
 * do not return a result. Instead, they're confined implementations of particular
 * command line major argument.
 *
 * Implementations may use [Name] annotation to override default naming
 * convention which is class name in lowercase.
 */
interface Command
{
    /**
     * Executes the command environment.
     */
    fun execute()

    fun validate(): Boolean
}