package lt.saltyjuice.dragas.utility.kommander.exception

class KommanderException @JvmOverloads constructor(message: String, cause: Throwable? = null) : Exception(message, cause)
{
}