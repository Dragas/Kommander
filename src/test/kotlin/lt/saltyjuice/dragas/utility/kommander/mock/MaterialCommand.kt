package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier
import lt.saltyjuice.dragas.utility.kommander.annotations.Name
import org.junit.Assert

@Name("material")
class MaterialCommand : TestCommand()
{
    @Modifier("a", "")
    private var aField: String = ""

    @Modifier("b")
    private var aBoolean: Boolean = false

    override fun execute()
    {
        Assert.assertEquals(providedField, aField)
        Assert.assertTrue(aBoolean)
    }

    companion object
    {
        @JvmStatic
        val providedField = "bbbd dddb"
    }
}