package lt.saltyjuice.dragas.utility.kommander

import org.junit.Assert

open class GenericTestContainer
{
    fun assertThatDoesNotThrow(lambda: () -> Unit)
    {
        Assert.assertFalse(testIfThrows(lambda))
    }

    fun assertThatThrows(lambda: () -> Unit)
    {
        Assert.assertTrue(testIfThrows(lambda))
    }

    private fun testIfThrows(lambda: () -> Unit): Boolean
    {
        return try
        {
            lambda()
            false
        }
        catch (err: Exception)
        {
            err.printStackTrace()
            true
        }
    }
}