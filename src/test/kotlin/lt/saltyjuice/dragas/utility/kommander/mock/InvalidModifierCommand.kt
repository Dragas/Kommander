package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier

class InvalidModifierCommand : TestCommand()
{
    @Modifier("i")
    fun objectModifier(any: Any)
    {

    }

    override fun execute()
    {

    }
}