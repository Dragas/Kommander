package lt.saltyjuice.dragas.utility.kommander

import lt.saltyjuice.dragas.utility.kommander.mock.InvalidModifierCommand
import lt.saltyjuice.dragas.utility.kommander.mock.MockWorkerBuilder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class InvalidModifierKommanderTest : GenericTestContainer()
{
    @Test
    fun kommanderDoesntParseInvalidCommand()
    {
        assertThatThrows { MockWorkerBuilder(InvalidModifierCommand::class.java).build() }
    }
}