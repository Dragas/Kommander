package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.main.Command
import lt.saltyjuice.dragas.utility.kommander.worker.Worker

class MockWorkerBuilder(clazz: Class<out Command>) : Worker.Builder(clazz)
{

}