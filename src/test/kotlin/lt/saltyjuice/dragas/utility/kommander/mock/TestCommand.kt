package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Name
import lt.saltyjuice.dragas.utility.kommander.main.Command

@Name("test")
open class TestCommand : Command
{
    override fun validate(): Boolean
    {
        return true
    }

    override fun execute()
    {
        println("yay!")
    }
}