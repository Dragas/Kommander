package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier

class ShadowingModifierCommand : TestCommand()
{
    @Modifier("-i")
    fun shadowed()
    {

    }

    @Modifier("-i")
    fun anotherShadowed()
    {

    }

    override fun execute()
    {

    }
}