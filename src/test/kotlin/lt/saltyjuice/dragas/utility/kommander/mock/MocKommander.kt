package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.main.Command
import lt.saltyjuice.dragas.utility.kommander.main.Kommander
import lt.saltyjuice.dragas.utility.kommander.worker.Worker

class MocKommander(vararg clazz: Class<out Command>) : Kommander(*clazz)
{

    override fun builder(clazz: Class<out Command>): Worker.Builder
    {
        return MockWorkerBuilder(clazz)
    }

    fun getCommandCount(): Int
    {
        return commands.size
    }

    fun has(name: String): Boolean
    {
        return commands.containsKey(name)
    }
}