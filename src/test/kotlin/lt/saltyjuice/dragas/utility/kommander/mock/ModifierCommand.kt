package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Description
import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier
import lt.saltyjuice.dragas.utility.kommander.annotations.Name
import org.junit.Assert

@Name("modifier")
@Description("Provides modifier fields for testing environment")
class ModifierCommand : TestCommand()
{
    private var intModifier: Int = -1
    private var charModifier: Char = '-'
    private var emptyModifier = false

    @Modifier("i", "-integer")
    @Description("sets integer value for this command")
    fun intModifier(int: Int)
    {
        intModifier = int
    }

    @Modifier("c", "-char")
    @Description("sets character value for this command")
    fun charModifier(c: Char)
    {
        charModifier = c
    }

    @Modifier("e", "-empty")
    @Description("sets boolean value for this command when called")
    fun emptyModifier()
    {
        emptyModifier = true
    }

    override fun execute()
    {
        Assert.assertTrue(emptyModifier)
        Assert.assertEquals(providedIntModifier, intModifier)
        Assert.assertEquals(providedCharModifier, charModifier)
        println("Modifier executed successfully with arguments: intModifier: $intModifier, charModifier: $charModifier, emptyModifier was called : ${emptyModifier}")
    }

    companion object
    {
        @JvmStatic
        val providedIntModifier = 1

        @JvmStatic
        val providedCharModifier = 'h'
    }
}