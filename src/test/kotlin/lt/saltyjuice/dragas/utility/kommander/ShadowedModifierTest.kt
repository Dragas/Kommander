package lt.saltyjuice.dragas.utility.kommander

import lt.saltyjuice.dragas.utility.kommander.mock.MockWorkerBuilder
import lt.saltyjuice.dragas.utility.kommander.mock.ShadowingModifierCommand
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ShadowedModifierTest : GenericTestContainer()
{
    @Test
    fun kommanderDoesntParseShadowedModifiers()
    {
        assertThatThrows { MockWorkerBuilder(ShadowingModifierCommand::class.java).build() }
    }
}