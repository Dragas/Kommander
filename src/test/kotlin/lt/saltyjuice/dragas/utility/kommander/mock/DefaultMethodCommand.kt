package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier
import lt.saltyjuice.dragas.utility.kommander.annotations.Name
import org.junit.Assert

@Name("default")
class DefaultMethodCommand : TestCommand()
{
    private var defaultField: Double = 5.0

    @Modifier("")
    fun defaultMethod(value: Double)
    {
        defaultField = value
    }

    override fun execute()
    {
        Assert.assertEquals(providedValue, defaultField, 0.0)
    }

    companion object
    {
        @JvmStatic
        val providedValue = 5.1
    }
}