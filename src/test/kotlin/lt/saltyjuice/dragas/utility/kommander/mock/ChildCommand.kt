package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Name

@Name("child")
class ChildCommand : ParentCommand()
{
    override fun validate(): Boolean
    {
        return true
    }
}