package lt.saltyjuice.dragas.utility.kommander.mock

import lt.saltyjuice.dragas.utility.kommander.annotations.Modifier
import lt.saltyjuice.dragas.utility.kommander.main.Command
import org.junit.Assert

abstract class ParentCommand : Command
{
    @Modifier("d")
    var default: Boolean = false

    @Modifier("df")
    @JvmField
    var defaultField: Boolean = false

    override fun execute()
    {
        Assert.assertTrue("Default argument was called: $default", default)
        Assert.assertTrue("Default argument field was called: $defaultField", defaultField)
    }
}