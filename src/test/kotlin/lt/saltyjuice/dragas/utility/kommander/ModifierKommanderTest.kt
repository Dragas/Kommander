package lt.saltyjuice.dragas.utility.kommander

import lt.saltyjuice.dragas.utility.kommander.mock.DefaultMethodCommand
import lt.saltyjuice.dragas.utility.kommander.mock.MaterialCommand
import lt.saltyjuice.dragas.utility.kommander.mock.MocKommander
import lt.saltyjuice.dragas.utility.kommander.mock.ModifierCommand
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.concurrent.Executors
import java.util.concurrent.Future

@RunWith(JUnit4::class)
class ModifierKommanderTest : GenericTestContainer()
{

    @Test
    fun kommanderHasParticularCommand()
    {
        val has = kommander.has("modifier")
        Assert.assertTrue(has)
    }

    @Test
    fun kommanderExecutesParticularCommand()
    {
        assertThatDoesNotThrow()
        {
            kommander.execute("modifier -i ${ModifierCommand.providedIntModifier} -c ${ModifierCommand.providedCharModifier} -e")
        }
    }

    @Test
    fun kommanderThrowsWithIncorrectArguments()
    {
        assertThatThrows()
        {
            kommander.execute("modifier --integer shit")
        }
    }

    @Test
    fun kommanderParsesFieldModifiersCorrectly()
    {
        assertThatDoesNotThrow()
        {
            kommander.execute("material -a ${MaterialCommand.providedField} -b")
        }
    }

    @Test
    fun kommanderSetsFieldValueCorrectly()
    {
        assertThatDoesNotThrow()
        {
            kommander.execute("material ${MaterialCommand.providedField} -b")
        }
    }

    @Test
    fun kommanderCanExecuteSameCallMultipleTimes()
    {
        assertThatDoesNotThrow()
        {
            kommander.execute("material ${MaterialCommand.providedField} -b")
            kommander.execute("material ${MaterialCommand.providedField} -b")
            kommander.execute("material ${MaterialCommand.providedField} -b")
            kommander.execute("material ${MaterialCommand.providedField} -b")
            kommander.execute("material ${MaterialCommand.providedField} -b")
        }
    }

    @Test
    fun kommanderCanExecuteSameCallMultipleTimesFromMultipleThreads()
    {
        val tg = Executors.newCachedThreadPool()
        val futures = mutableListOf<Future<*>>()
        for (i in 0..5)
        {
            futures += tg.submit()
            {
                assertThatDoesNotThrow()
                {
                    kommander.execute("material ${MaterialCommand.providedField} -b")
                }
            }
        }
        futures.forEach()
        {
            assertThatDoesNotThrow()
            {
                it.get()
            }
        }
    }


    @Test
    fun kommanderCanExecuteSameCallFromMultipleThreads()
    {
        val thread1 = Thread({
            assertThatDoesNotThrow {
                kommander.execute("material ${MaterialCommand.providedField} -b")
            }
        })
        val thread2 = Thread({
            assertThatDoesNotThrow {
                kommander.execute("material ${MaterialCommand.providedField} -b")
            }
        })
        thread1.start()
        thread2.start()
        thread1.join()
        thread2.join()
    }

    @Test
    fun kommanderSetsDefaultMethodCorrectly()
    {
        assertThatDoesNotThrow()
        {
            kommander.execute("default ${DefaultMethodCommand.providedValue}")
        }
    }

    companion object
    {
        @JvmStatic
        private val kommander: MocKommander = MocKommander(ModifierCommand::class.java, MaterialCommand::class.java, DefaultMethodCommand::class.java)

        @BeforeClass
        @JvmStatic
        fun init()
        {
            kommander.initialize()
        }
    }
}