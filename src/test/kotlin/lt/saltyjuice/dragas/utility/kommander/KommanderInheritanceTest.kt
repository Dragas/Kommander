package lt.saltyjuice.dragas.utility.kommander

import lt.saltyjuice.dragas.utility.kommander.main.Kommander
import lt.saltyjuice.dragas.utility.kommander.mock.ChildCommand
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class KommanderInheritanceTest : GenericTestContainer()
{
    @Test
    fun commandsCanInherit()
    {
        val kommander = Kommander(ChildCommand::class.java).initialize()
        assertThatDoesNotThrow {
            kommander.execute("child -d -df")
        }
    }
}