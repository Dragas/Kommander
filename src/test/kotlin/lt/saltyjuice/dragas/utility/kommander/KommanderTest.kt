package lt.saltyjuice.dragas.utility.kommander

import lt.saltyjuice.dragas.utility.kommander.mock.MocKommander
import lt.saltyjuice.dragas.utility.kommander.mock.TestCommand
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class KommanderTest : GenericTestContainer()
{

    @Test
    fun kommanderParsesClassesCorrectly()
    {
        val count = kommander.getCommandCount()
        Assert.assertEquals(count, 1)
    }

    @Test
    fun kommanderHasTestCommand()
    {
        val hasTest = kommander.has("test")
        Assert.assertTrue(hasTest)
    }

    @Test
    fun kommanderCanExecuteTestCommand()
    {
        assertThatDoesNotThrow()
        {
            kommander.execute("test")
        }
    }

    @Test
    fun kommanderCantExecuteNotExistantCommand()
    {
        assertThatThrows()
        {
            kommander.execute("")
        }
    }

    @Test
    fun kommanderDescriptionIsCorrect()
    {
        val sb = StringBuilder()
        sb.appendln("test")
        Assert.assertEquals(sb.toString(), kommander.description)
    }

    companion object
    {
        @JvmStatic
        private val kommander: MocKommander = MocKommander(TestCommand::class.java)

        @BeforeClass
        @JvmStatic
        fun init()
        {
            kommander.initialize()
        }
    }
}