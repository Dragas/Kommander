# 1.1.0

### Changes

- Kommander now checks for inheritence and parses all the fields down the inheritance chain.
- `@Modifier` is now applicable to method setters.
- Fixes the issue where fields couldn't be private.
- Refactored how parameter casting for `Tool`s works. Now they try to map each parameter to corresponding
argument provided in argument array.

# 1.0.1

### Changes

- Fixed an issue where default arguments would get incorrectly parsed.
    - As a result, `Worker.parseModifier(Command, List<String>)` was changed to `Worker.parseModifier(Command, String)`
- Fixed an issue where `Tool` parameters would get parsed incorrectly in descriptions.


# 1.0.0

Release. It doesn't differ that much from 0.2.0

# 0.2.0-SNAPSHOT

Mainly refactors spaghetti.

### Added

- Materials and Tools
    - Materials are Fields annotated with "@Modifier" annotation. This change permits using Modifier
    annotations on fields
    - Tools are Methods annotated with "@Modifier" annotation. This separates casting logic from workers
    themselves.

Both of them inherit from "Modifiable". 

### Changed

- WorkerBuilder is now Worker.Builder.

# 0.1.1-SNAPSHOT

Initializes project.

### Added

- Kommander.
    - Kommander handles all the workers provided to that instance. Basically works like a factory:
    chooses which worker should be invoked for particular "command line"
- Workers.
    - Workers act like method factory for their Command instance. They choose which methods should be invoked
    while building that command before its execution.
- Command
    - Commands are actually both Builders and Executors. All they do is "hold" values that are passed via
    "command line" and bound via methods that are with "Modifier"
