# Kommander

Command line like apps simplified.

[![pipeline status](https://gitlab.com/Dragas/Kommander/badges/master/pipeline.svg)](https://gitlab.com/Dragas/Kommander/commits/master)
[![coverage report](https://gitlab.com/Dragas/Kommander/badges/master/coverage.svg)](https://gitlab.com/Dragas/Kommander/commits/master)

## Adding the bling

All you need to do is extend `Command` interface and pass it to your `Kommander` instance!

```kotlin
class Help : Command
```
Adding modifiers and arguments to your commands is as simple as just adding an annotation
```kotlin
// in Help
@Modifier("v", "-verbose")
fun verbose() // makes your command 'help -v' or 'help --verbose'
``` 
By default, Kommander looks for any modifiers starting with '-', splits the argument line there and does its thing.

You may also provide arguments for your modifying method
```kotlin
@Modifier("f", "-file")
fun file(filename : String) // makes your command become 'help -f file.cpp' or 'help --file file.cpp'
```
The argument types need to be primitive values such as Boolean, Integer, Float, Double, String, Char, Short, Byte and Long.
Kommander will pass them to respective methods, where you can handle them. If you intend on having methods with multiple arguments,
each argument beyond the first needs to be nullable, otherwise you'll get an invalid argument exception.

You may also use field modifiers.
```kotlin
@Modifier("a", "-all")
private var selectsAll : Boolean = false // makes your command become 'help -a' 
```
This would set the `selectAll` field to true when `-a` modifier is provided. For other primitive types,
their provided value is assigned.

If you were to set `@Modifier("")`, then that means that it's the default argument for that particular command.

You can also add documentation to your modifying fields and/or commands themselves. Just add `@Description` annotation
to your target and Kommander will handle it for you. To read generated documentation call `kommander.description` or `worker.description`
on respective objects.

## Downloading

Add sonatype as your checked repositories
```groovy
repositories {
    maven { url "https://oss.sonatype.org/content/repositories/releases" }
    maven { url "https://oss.sonatype.org/content/repositories/snapshots" }
}
```
And then just list `Kommander` as your dependency.
```groovy
compile "lt.saltyjuice.dragas.utility:kommander:1.1.0"
```